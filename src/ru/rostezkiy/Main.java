package ru.rostezkiy;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("> \n");
        while (true) {
            String str;
            String reverseStr;
            Scanner input = new Scanner(System.in);
            str = input.nextLine();
            reverseStr = new StringBuffer(str).reverse().toString();
            System.out.print(reverseStr + "\n > ");
        }
    }
}